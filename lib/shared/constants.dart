import 'package:flutter/material.dart';

const kPrimaryColor = Colors.black87;
const kPrimaryLightColor = Color(0xFF8FEA6A);
