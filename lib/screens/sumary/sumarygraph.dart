import 'dart:async';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:clovery/models/Session.dart';
import 'package:clovery/services/DatabaseService.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class GraphChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Session>>.value(
        value: DatabaseService().sessions, child: GraphData());
  }
}

class GraphData extends StatefulWidget {
  GraphData({Key key}) : super(key: key);

  @override
  _GraphDataState createState() => _GraphDataState();
}

class _GraphDataState extends State<GraphData> {
  List<charts.Series<Session, int>> _seriesLineData;
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  _updateData(List<Session> sessions) {
    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: 'Session noise',
        data: sessions,
        domainFn: (Session noise, _) => noise.time,
        measureFn: (Session noise, _) => noise.value,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _connectionStatus = 'conected';
        break;
      case ConnectivityResult.none:
        _connectionStatus = 'disconected';
        break;
      default:
        _connectionStatus = 'Failed to get connectivity.';
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final sesions = Provider.of<List<Session>>(context) ?? [];
    _seriesLineData = List<charts.Series<Session, int>>();
    _updateData(sesions);
    if (_connectionStatus == 'conected' || sesions.isNotEmpty) {
      return Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                'Noise in study sesions',
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              ),
              Expanded(
                child: charts.LineChart(_seriesLineData,
                    defaultRenderer: new charts.LineRendererConfig(
                        includeArea: true, stacked: true),
                    animate: true,
                    animationDuration: Duration(seconds: 1),
                    behaviors: [
                      new charts.ChartTitle('Time',
                          behaviorPosition: charts.BehaviorPosition.bottom,
                          titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                      new charts.ChartTitle('Decibels',
                          behaviorPosition: charts.BehaviorPosition.start,
                          titleOutsideJustification:
                              charts.OutsideJustification.middleDrawArea),
                    ]),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: Image(
          image: AssetImage('assets/images/sad.png'),
          height: 100,
          width: 100,
        ),
      );
    }
  }
}
