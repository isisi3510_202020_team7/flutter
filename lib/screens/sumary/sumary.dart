import 'package:clovery/screens/sumary/sumarybar.dart';
import 'package:clovery/screens/sumary/sumarygraph.dart';
import 'package:clovery/screens/sumary/sumarypie.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:clovery/shared/constants.dart';

class Sumary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kPrimaryLightColor,
          bottom: TabBar(
            indicatorColor: Color(0xff9962D0),
            tabs: [
              Tab(
                icon: Icon(FontAwesomeIcons.solidChartBar),
              ),
              Tab(icon: Icon(FontAwesomeIcons.chartPie)),
              Tab(icon: Icon(FontAwesomeIcons.chartLine)),
            ],
          ),
          title: Text('Your Sumary'),
        ),
        body: TabBarView(
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(child: Barchart()),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(child: Piechart()),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Container(child: GraphChart()),
            ),
          ],
        ),
      ),
    );
  }
}
