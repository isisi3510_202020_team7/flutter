import 'dart:async';

import 'package:clovery/models/Activity.dart';
import 'package:clovery/services/DatabaseService.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class Piechart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<List<Activity>>.value(
            value: DatabaseService().activities),
      ],
      child: PieData(),
    );
  }
}

class PieData extends StatefulWidget {
  PieData({Key key}) : super(key: key);

  @override
  _PieDataState createState() => _PieDataState();
}

class _PieDataState extends State<PieData> {
  List<charts.Series<Activity, String>> _seriesPieData;
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  _updateData(List<Activity> activities) {
    _seriesPieData.add(
      charts.Series(
        domainFn: (Activity activity, _) => activity.name,
        measureFn: (Activity activity, _) => activity.value,
        colorFn: (Activity task, _) =>
            charts.ColorUtil.fromDartColor(Color(task.color)),
        id: 'Pie Chart ',
        data: activities,
        labelAccessorFn: (Activity row, _) => '${row.value}',
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _connectionStatus = 'conected';
        break;
      case ConnectivityResult.none:
        _connectionStatus = 'disconected';
        break;
      default:
        _connectionStatus = 'Failed to get connectivity.';
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final activities = Provider.of<List<Activity>>(context) ?? [];
    _seriesPieData = List<charts.Series<Activity, String>>();
    if (_connectionStatus == 'conected' || activities.isNotEmpty) {
      _updateData(activities);
      return Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                'Time spent on daily tasks',
                style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0,
              ),
              Expanded(
                child: charts.PieChart(_seriesPieData,
                    animate: true,
                    animationDuration: Duration(seconds: 1),
                    behaviors: [
                      new charts.DatumLegend(
                        outsideJustification:
                            charts.OutsideJustification.endDrawArea,
                        horizontalFirst: false,
                        desiredMaxRows: 2,
                        cellPadding:
                            new EdgeInsets.only(right: 4.0, bottom: 4.0),
                        entryTextStyle: charts.TextStyleSpec(
                            color: charts.MaterialPalette.purple.shadeDefault,
                            fontFamily: 'Georgia',
                            fontSize: 11),
                      )
                    ],
                    defaultRenderer: new charts.ArcRendererConfig(
                        arcWidth: 100,
                        arcRendererDecorators: [
                          new charts.ArcLabelDecorator(
                              labelPosition: charts.ArcLabelPosition.inside)
                        ])),
              ),
            ],
          ),
        ),
      );
    } else {
      return Container(
        child: Image(
          image: AssetImage('assets/images/sad.png'),
          height: 100,
          width: 100,
        ),
      );
    }
  }
}
