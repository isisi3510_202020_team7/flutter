import 'package:clovery/models/Distribution.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class Barchart extends StatefulWidget {
  Barchart({Key key}) : super(key: key);

  @override
  _BarchartState createState() => _BarchartState();
}

class _BarchartState extends State<Barchart> {
  List<charts.Series<Distribution, String>> _seriesData;

  _generateData() {
    var data1 = [
      new Distribution('Jan', 30),
      new Distribution('Feb', 40),
      new Distribution('Mar', 10),
    ];
    var data2 = [
      new Distribution('Jan', 100),
      new Distribution('Feb', 90),
      new Distribution('Mar', 80),
    ];
    var data3 = [
      new Distribution('Jan', 200),
      new Distribution('Feb', 210),
      new Distribution('Mar', 100),
    ];

    _seriesData.add(
      charts.Series(
        domainFn: (Distribution dist, _) => dist.month,
        measureFn: (Distribution dist, _) => dist.time,
        id: '2017',
        data: data1,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Distribution dist, _) =>
            charts.ColorUtil.fromDartColor(Color(0xff990099)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Distribution pollution, _) => pollution.month,
        measureFn: (Distribution pollution, _) => pollution.time,
        id: '2018',
        data: data2,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Distribution pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xff109618)),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Distribution pollution, _) => pollution.month,
        measureFn: (Distribution pollution, _) => pollution.time,
        id: '2019',
        data: data3,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Distribution pollution, _) =>
            charts.ColorUtil.fromDartColor(Color(0xffff9900)),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _seriesData = List<charts.Series<Distribution, String>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Text(
              'Time Distribution Over 3 Months',
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
            ),
            Expanded(
              child: charts.BarChart(
                _seriesData,
                animate: true,
                barGroupingType: charts.BarGroupingType.grouped,
                animationDuration: Duration(seconds: 1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
