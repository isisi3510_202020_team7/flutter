import 'package:clovery/shared/constants.dart';
import 'package:flutter/material.dart';

class Summary extends StatelessWidget {
  final int score;
  final String image;
  Summary({Key key, @required this.score, @required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => true,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Clovery'),
          backgroundColor: kPrimaryLightColor,
        ),
        body: new Container(
          alignment: Alignment.topCenter,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Text(
                "Final Score: $score",
                style: new TextStyle(fontSize: 35.0),
              ),
              SizedBox(
                height: 50,
              ),
              Image(
                image: AssetImage(image),
                height: 150,
                width: 150,
              ),
              new Padding(padding: EdgeInsets.all(30.0)),
              new MaterialButton(
                color: kPrimaryLightColor,
                onPressed: () {
                  Navigator.pop(context);
                },
                child: new Text(
                  "New Quiz",
                  style: new TextStyle(fontSize: 20.0, color: Colors.white),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
