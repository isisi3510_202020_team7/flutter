import 'package:clovery/models/Question.dart';
import 'package:clovery/models/Quiz.dart';
import 'package:clovery/screens/exercises/results.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:clovery/shared/constants.dart';

class Exercises extends StatefulWidget {
  Exercises({Key key}) : super(key: key);

  @override
  _ExercisesState createState() => _ExercisesState();
}

class _ExercisesState extends State<Exercises> {
  var questionNumber;
  var finalScore;
  var quiz;
  var correct;
  var image;
  final smile = 'assets/images/smile.png';
  final sad = 'assets/images/sad.png';
  final neutral = 'assets/images/neutral.png';
  final answer = TextEditingController();

  _ExercisesState() {
    image = smile;
    questionNumber = 0;
    correct = 0;
    finalScore = 0;
    Question question1 = new Question(number1: 34, number2: 17, operation: '+');
    Question question2 = new Question(number1: 33, number2: 15, operation: '-');
    Question question3 = new Question(number1: 21, number2: 6, operation: 'x');
    Question question4 = new Question(number1: 81, number2: 9, operation: '÷');
    Question question5 =
        new Question(number1: 44, number2: 305, operation: '+');
    Question question6 = new Question(number1: 84, number2: 60, operation: 'x');
    Question question7 =
        new Question(number1: 109, number2: 607, operation: '+');
    Question question8 = new Question(number1: 256, number2: 2, operation: '÷');
    List<Question> list = new List();
    list.add(question1);
    list.add(question2);
    list.add(question3);
    list.add(question4);
    list.add(question5);
    list.add(question6);
    list.add(question7);
    list.add(question8);
    quiz = Quiz(questions: list);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: new Container(
            margin: const EdgeInsets.all(10.0),
            alignment: Alignment.topCenter,
            child: new Column(
              children: <Widget>[
                new Padding(padding: EdgeInsets.all(20.0)),
                new Container(
                    alignment: Alignment.centerRight,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Text(
                          "Question ${questionNumber + 1} of ${quiz.questions.length}",
                          style: new TextStyle(fontSize: 22.0),
                        ),
                        new Text(
                          "Score: $finalScore",
                          style: new TextStyle(fontSize: 22.0),
                        )
                      ],
                    )),
                new Column(
                  children: [
                    SizedBox(height: 30),
                    new Text(
                      '¿Cual es el resultado?',
                      style: new TextStyle(fontSize: 25.0),
                    ),
                    SizedBox(height: 15),
                    new Text(
                      quiz.questions[questionNumber].toString(),
                      style: new TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                    SizedBox(height: 35),
                    Image(
                      image: AssetImage(image),
                      height: 60,
                      width: 60,
                    ),
                    Container(
                      width: 150,
                      child: new TextField(
                        decoration: new InputDecoration(
                            labelText: "Escribe tu respuesta"),
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        controller: answer,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              if (quiz.questions[questionNumber].result() ==
                  int.parse(answer.text)) {
                correct++;
              }
              finalScore = (correct / (questionNumber + 1) * 100).round();
              updateQuestion();
            },
            child: Icon(Icons.arrow_forward),
            backgroundColor: kPrimaryLightColor,
          ),
        ),
      ),
    );
  }

  void updateQuestion() {
    setState(() {
      if (questionNumber == quiz.questions.length - 1) {
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new Summary(
                      score: finalScore,
                      image: image,
                    )));
      } else {
        if (finalScore >= 80) {
          image = smile;
        } else if (finalScore < 80 && finalScore >= 60) {
          image = neutral;
        } else
          image = sad;
        questionNumber++;
      }
    });
  }
}
