import 'package:clovery/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:clovery/shared/constants.dart';
import 'package:clovery/components/rounded_input_field.dart';
import 'package:clovery/components/rounded_password_field.dart';
import 'package:clovery/components/rounded_button.dart';

class Login extends StatefulWidget {
  final Function callbackToRegister;

  Login({this.callbackToRegister});

  // Auth service
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  // Auth Service
  final AuthService _auth = AuthService();

  // Form Key
  final _formKey = GlobalKey<FormState>();

  // User inputs
  String email = '';
  String password = '';

  // Errors
  String error = '';

  // Error handling
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Text("Alert"),
          content: Text(error),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: size.height,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "CLOVERY",
                      style: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: 20,
                        letterSpacing: 3,
                      ),
                    ),
                    Flexible(
                      child: Image(
                        image: AssetImage('assets/images/clovery_logo2.png'),
                        height: size.height * 0.3,
                        width: size.height * 0.3,
                      ),
                    ),
                    RoundedInputField(
                      hintText: "Your Email",
                      onChanged: (value) {
                        email = value;
                      },
                      icon: Icons.email,
                      givenValidator: (value) {
                        return _auth.validateEmail(value);
                      },
                    ),
                    RoundedPasswordField(
                      onChanged: (value) {
                        password = value;
                      },
                      givenValidator: (value) {
                        return _auth.validatePassword(value);
                      },
                    ),
                    RoundedButton(
                      text: "LOGIN",
                      press: () async {
                        if (_formKey.currentState.validate()) {
                          dynamic firebaseResponse =
                              await _auth.signIn(email, password);
                          if (firebaseResponse == null) {
                            setState(() {
                              error =
                                  "Error during the sign up process. Try again";
                            });
                            _showDialog();
                          }
                        }
                      },
                    ),
                    SizedBox(height: size.height * 0.03),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Don’t have an Account? ",
                          style: TextStyle(color: kPrimaryColor),
                        ),
                        GestureDetector(
                          onTap: () {
                            widget.callbackToRegister("/register");
                          },
                          child: Text(
                            "Sign Up",
                            style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
