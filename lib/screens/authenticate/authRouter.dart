import 'package:clovery/screens/authenticate/login.dart';
import 'package:clovery/screens/authenticate/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:clovery/screens/home/home.dart';
import 'package:clovery/screens/authenticate/welcome.dart';
import 'package:provider/provider.dart';

class AuthRouter extends StatefulWidget {
  @override
  _AuthRouterState createState() => _AuthRouterState();
}

class _AuthRouterState extends State<AuthRouter> {
  String currentRoute = '/welcome';

  modifyRoute(String newRoute) {
    setState(() {
      currentRoute = newRoute;
    });
  }

  @override
  Widget build(BuildContext context) {
    User cloveryUser = Provider.of<User>(context);
    if (cloveryUser == null) {
      if (currentRoute == "/welcome") {
        return Welcome(
          callbackToLogin: modifyRoute,
          callbackToRegister: modifyRoute,
        );
      } else if (currentRoute == "/register") {
        return Register(
          callbackToLogin: modifyRoute,
        );
      } else if (currentRoute == "/login") {
        return Login(
          callbackToRegister: modifyRoute,
        );
      } else {
        return Welcome(
          callbackToLogin: modifyRoute,
          callbackToRegister: modifyRoute,
        );
      }
    } else {
      modifyRoute("/welcome");
      return Home();
    }
  }
}
