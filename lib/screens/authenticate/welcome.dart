import 'package:flutter/material.dart';
import 'package:clovery/shared/constants.dart';
import 'package:clovery/components/rounded_button.dart';

class Welcome extends StatelessWidget {
  final Function callbackToLogin;
  final Function callbackToRegister;

  Welcome({this.callbackToLogin, this.callbackToRegister});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: size.height,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "CLOVERY",
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                      letterSpacing: 3,
                    ),
                  ),
                  Flexible(
                    child: Image(
                      image: AssetImage('assets/images/clovery_logo2.png'),
                      height: size.height * 0.3,
                      width: size.height * 0.3,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Text(
                          "The solution to all your concentration and time management problems.",
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: size.height * 0.05),
                  RoundedButton(
                    text: "LOGIN",
                    color: kPrimaryLightColor,
                    textColor: Colors.black,
                    press: () {
                      callbackToLogin("/login");
                    },
                  ),
                  RoundedButton(
                    text: "SIGN UP",
                    press: () {
                      callbackToRegister("/register");
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
