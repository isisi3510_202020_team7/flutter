import 'package:clovery/screens/exercises/exercises.dart';
import 'package:clovery/screens/activities/activities.dart';
import 'package:clovery/screens/focusmode/home_page.dart';
import 'package:clovery/screens/sumary/sumary.dart';
import 'package:clovery/screens/user/user.dart';
import 'package:flutter/material.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:clovery/shared/constants.dart';

class Home extends StatelessWidget {
  final List<TabItem<dynamic>> _navBarItems = [
    TabItem(icon: Icons.playlist_add_check, title: 'Summary'),
    TabItem(icon: Icons.school, title: 'Exercise'),
    TabItem(icon: Icons.home, title: 'Home'),
    TabItem(icon: Icons.spa, title: 'Activities'),
    TabItem(icon: Icons.person, title: 'User'),
  ];

  final List<Widget> _screens = [
    Sumary(),
    Exercises(),
    Homepage(),
    ActivityList(),
    Userpage(),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: _navBarItems.length,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0,
          title: Text(
            "CLOVERY",
            style: TextStyle(
              color: Colors.black87,
              letterSpacing: 3,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: SafeArea(
          child: TabBarView(
            children: _screens,
          ),
        ),
        bottomNavigationBar: ConvexAppBar(
          backgroundColor: kPrimaryLightColor,
          items: _navBarItems,
          initialActiveIndex: 2,
          //onTap: (int i) => print('click index=$i'), se puede usar para avisar al usuario que intenta salir cuando esta en modo de estudio
        ),
      ),
    );
  }
}
