import 'dart:async';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:clovery/shared/constants.dart';

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  Timer _timer;
  double progressValue = 0;
  // ignore: sort_constructors_first
  _LoadingState() {
    _timer = Timer.periodic(const Duration(milliseconds: 20), (Timer _timer) {
      print(progressValue);
      setState(() {
        progressValue++;
        if (progressValue == 100) {
          _timer.cancel();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SfRadialGauge(
        axes: <RadialAxis>[
          RadialAxis(
            pointers: [
              RangePointer(
                color: kPrimaryLightColor,
                value: progressValue,
                cornerStyle: CornerStyle.bothCurve,
                width: 0.2,
                sizeUnit: GaugeSizeUnit.factor,
                enableAnimation: true,
                animationDuration: 100,
                animationType: AnimationType.linear,
              ),
            ],
            annotations: [
              GaugeAnnotation(
                positionFactor: 0.1,
                angle: 90,
                widget: Text(
                  '${progressValue.toStringAsFixed(0)} / 100 loading...',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ],
            minimum: 0,
            maximum: 100,
            showLabels: false,
            showTicks: false,
            axisLineStyle: AxisLineStyle(
              thickness: 0.2,
              cornerStyle: CornerStyle.bothCurve,
              color: Colors.black87,
              thicknessUnit: GaugeSizeUnit.factor,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
