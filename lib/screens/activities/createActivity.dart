import 'package:clovery/services/databaseservice.dart';
import 'package:clovery/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:clovery/models/User.dart';

class CreateActivity extends StatefulWidget {
  CreateActivity({Key key}) : super(key: key);

  @override
  _CreateActivityState createState() => _CreateActivityState();
}

class _CreateActivityState extends State<CreateActivity> {
  int color1 = Colors.green.value;
  int color2 = Colors.white.value;
  int color3 = Colors.cyan.value;
  int color4 = Colors.deepOrange.value;
  String dropdownValue = 'Green';
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _description;
  int _value;
  int _color;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: kPrimaryLightColor,
        title: new Text("Create a new Activity"),
        actions: <Widget>[
          new IconButton(
              icon: const Icon(Icons.save),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  await DatabaseService()
                      .addActivity(_name, _description, _value, _color);
                  Navigator.pop(context);
                }
              })
        ],
      ),
      body: new Column(
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          new Text('Activity Name'),
          new ListTile(
            leading: const Icon(Icons.work),
            title: new TextFormField(
              decoration: new InputDecoration(
                hintText: "Name",
              ),
              validator: (val) => val.isEmpty ? 'Please enter a name' : null,
              onChanged: (val) => setState(() => _description = val),
            ),
          ),
          SizedBox(height: 15),
          new Text('Activity Description'),
          new ListTile(
            leading: const Icon(Icons.text_fields),
            title: new TextFormField(
              decoration: new InputDecoration(
                hintText: "Description",
              ),
              validator: (val) =>
                  val.isEmpty ? 'Please enter a description' : null,
              onChanged: (val) => setState(() => _description = val),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          new Text('Time consumed'),
          new ListTile(
            leading: const Icon(Icons.timer),
            title: new TextFormField(
              decoration: new InputDecoration(
                hintText: "Time",
              ),
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              validator: (val) =>
                  val.isEmpty ? 'Please enter the time you spended' : null,
              onChanged: (val) => setState(() => _value = val as int),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          new Text('Color for the activity'),
          new DropdownButton(
            value: dropdownValue,
            icon: Icon(Icons.arrow_downward),
            iconSize: 24,
            elevation: 16,
            style: TextStyle(color: kPrimaryLightColor),
            underline: Container(
              height: 2,
              color: kPrimaryLightColor,
            ),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
                switch (newValue) {
                  case 'Green':
                    _color = color1;
                    break;
                  case 'White':
                    _color = color2;
                    break;
                  case 'Cyan':
                    _color = color3;
                    break;
                  case 'Orange':
                    _color = color4;
                    break;
                  default:
                    _color = color1;
                }
              });
            },
            items: <String>['Green', 'White', 'Cyan', 'Orange']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
