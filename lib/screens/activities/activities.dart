import 'dart:async';

import 'package:clovery/models/Activity.dart';
import 'package:clovery/screens/activities/createActivity.dart';
import 'package:clovery/services/databaseservice.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

import 'package:clovery/shared/constants.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class ActivityList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<List<Activity>>.value(
            value: DatabaseService().activities),
      ],
      child: Activities(),
    );
  }
}

class Activities extends StatefulWidget {
  @override
  _ActivitiesState createState() => _ActivitiesState();
}

class _ActivitiesState extends State<Activities> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _connectionStatus = 'conected';
        break;
      case ConnectivityResult.none:
        _connectionStatus = 'disconected';
        break;
      default:
        _connectionStatus = 'Failed to get connectivity.';
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    final activities = Provider.of<List<Activity>>(context) ?? [];
    if (_connectionStatus == 'conected' || activities.isNotEmpty) {
      return Scaffold(
        backgroundColor: Colors.white,
        body: GridView.count(
            // Create a grid with 2 columns. If you change the scrollDirection to
            // horizontal, this produces 2 rows.
            crossAxisCount: 2,
            // Generate 100 widgets that display their index in the List.
            children: List.generate(
              activities.length,
              (index) {
                return Center(
                    child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      ListTile(
                        leading: Icon(Icons.watch),
                        title: Text(activities[index].name),
                        subtitle: Text(activities[index].value.toString()),
                      ),
                    ],
                  ),
                  color: Color(activities[index].color),
                ));
              },
            )),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => new CreateActivity()));
          },
          label: Text('Create'),
          icon: Icon(Icons.create),
          backgroundColor: kPrimaryLightColor,
        ),
      );
    } else {
      return Container(
        child: Image(
          image: AssetImage('assets/images/sad.png'),
          height: 100,
          width: 100,
        ),
      );
    }
  }
}
