import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:clovery/models/Activity.dart';
import 'package:clovery/models/Session.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';

class DatabaseService {
  final String uid;
  DatabaseService({this.uid});

  final CollectionReference activitiescollection =
      FirebaseFirestore.instance.collection('activity');

  final CollectionReference sessionscollection =
      FirebaseFirestore.instance.collection('session');

  Future<void> addActivity(
      String name, String description, int value, int color) async {
    var map = {
      'name': name,
      'description': description,
      'value': value,
      'color': color,
      'uid': uid
    };
    return await activitiescollection.add(map);
  }

  Future<void> updateActivity(Activity activity) async {
    var map = {
      'name': activity.name,
      'description': activity.description,
      'value': activity.value,
      'color': activity.color,
      'uid': uid
    };
    return await activitiescollection.doc(activity.id).set(map);
  }

  List<Activity> _activitiesSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return Activity(
          id: doc.id,
          name: doc.data()['name'],
          description: doc.data()['description'],
          value: doc.data()['value'],
          color: doc.data()['color']);
    }).toList();
  }

  Stream<List<Activity>> get activities {
    return activitiescollection
        .where('uid', isEqualTo: uid)
        .snapshots()
        .map(_activitiesSnapshot);
  }

  Future<void> deleteActivity(Activity activity) async {
    return await activitiescollection.doc(activity.id).delete();
  }

  Future<void> addSession(int value, int time) async {
    var map = {'value': value, 'time': time, 'uid': uid};
    return await sessionscollection.add(map);
  }

  Future<void> updateSession(Session session) async {
    var map = {'value': session.value, 'time': session.time, 'uid': uid};
    return await activitiescollection.doc(uid).set(map);
  }

  List<Session> _sessionsSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return Session(
        value: doc.data()['value'],
        time: doc.data()['time'],
      );
    }).toList();
  }

  Stream<List<Session>> get sessions {
    return sessionscollection
        .where('uid', isEqualTo: uid)
        .snapshots()
        .map(_sessionsSnapshot);
  }

  Future<void> deleteSession(Session session) async {
    return sessionscollection.doc(session.uid).delete();
  }
}
