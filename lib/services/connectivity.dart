import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class ConnectivityService {
  var _connectivity;

  bool mounted;

  String _connectionStatus;

  ConnectivityService() {
    _connectionStatus = 'Unknown';
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return updateConnectionStatus(result);
  }

  Future<void> updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        _connectionStatus = 'conected';
        debugPrint('connece');
        break;
      case ConnectivityResult.none:
        _connectionStatus = 'disconected';
        break;
      default:
        _connectionStatus = 'Failed to get connectivity.';
        break;
    }
  }

  String connectionStatus() {
    return _connectionStatus;
  }
}
