import 'package:clovery/models/Activity.dart';
import 'package:clovery/models/User.dart';
import 'package:clovery/services/databaseservice.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthService {
  // Firebase Auth Instance
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Get cloveryUser object from Firebase user
  CloveryUser _cloveryUserFromFirebaseUser(User firebaseUser) {
    return firebaseUser != null ? CloveryUser(uid: firebaseUser.uid) : null;
  }

  // Get user logged in from Firebase
  Stream<User> get user {
    return _auth.authStateChanges();
  }

  // Sign in with email and password
  Future<dynamic> signIn(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      User user = userCredential.user;
      return user;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // Register with email and password
  Future<CloveryUser> registerUser(String email, String password) async {
    try {
      UserCredential userCredential =
          await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      User user = userCredential.user;
      List<Activity> _list;
      _list.add(Activity(
          name: 'Work',
          description: 'Issues resolved',
          value: 50,
          color: 0xff3366cc));
      _list.add(Activity(
          name: 'Sleep',
          description: 'Time resting',
          value: 50,
          color: 0xffff9900));
      _list.forEach((element) async {
        await DatabaseService(uid: user.uid).updateActivity(_list[0]);
      });

      return _cloveryUserFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // Log out
  Future logOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // Authentication Validators
  String validateName(String value) {
    if (value.length < 5)
      return 'Name must be more than 5 characters.';
    else
      return null;
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter a valid Email.';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.length < 6)
      return 'Password must be more than 6 characters.';
    else
      return null;
  }
}
