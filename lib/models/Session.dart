class Session {
  final int value;
  final int time;
  final String uid;

  Session({this.value, this.time, this.uid});
}
