class Question {
  final int number1;
  final int number2;
  final String operation;

  @override
  String toString() {
    return number1.toString() +
        ' ' +
        operation +
        ' ' +
        number2.toString() +
        ' = ___';
  }

  int result() {
    switch (operation) {
      case '+':
        {
          return number1 + number2;
        }
      case '-':
        {
          return number1 - number2;
        }
      case 'x':
        {
          return number1 * number2;
        }
      case '÷':
        {
          return (number1 / number2).round();
        }
      default:
        {
          return -1;
        }
    }
  }

  Question({this.number1, this.number2, this.operation});
}
