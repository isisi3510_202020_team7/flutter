import 'package:clovery/models/Activity.dart';

class Collection {
  final List<Activity> activities;
  final String name;

  Collection({this.name, this.activities});
}
