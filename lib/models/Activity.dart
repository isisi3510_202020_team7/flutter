class Activity {
  final String id;
  final String name;
  final String description;
  final double value;
  final int color;

  Activity({this.id, this.name, this.description, this.value, this.color});
}
