import 'package:clovery/models/Question.dart';

class Quiz {
  final List<Question> questions;

  Quiz({this.questions});
}
